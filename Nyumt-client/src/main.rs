/*
Tiny CRM: A port of the Yew CRM example to Dioxus.
*/
use dioxus::{events::FormEvent, prelude::*};

fn main() {
    dioxus::desktop::launch(app);
}
enum Scene {
    databaseList,
    NewDatabaseForm,
    Settings,
    keyFil,
}
pub enum Color {
    Green,
    Yellow,
    Red,
}

#[derive(Clone, Debug, Default)]
pub struct Database {
    pub Enter_passwords: String,
    pub Confir_mpasswords: String,
    pub key_File: String,
    
}

fn app(cx: Scope) -> Element {
    let scene = use_state(&cx, || Scene::databaseList);
    let database = use_ref(&cx, || vec![] as Vec<Database>);
    let Enterpasswords = use_state(&cx, String::new);
    let Confirmpasswords = use_state(&cx, String::new);
    let keyFile = use_state(&cx, String::new);
    let value=use_state(&cx,||String::from("50"));
    let password_verify = use_state(&cx,||String::from("egale"));
    
    

    cx.render(rsx!(
        body {
           
            link {
                rel: "stylesheet",
                href: "https://unpkg.com/purecss@2.0.6/build/pure-min.css",
            }
            link {
                rel:"stylesheet" ,
                href:"https://fonts.googleapis.com/css?family=Sofia",
            }
            link {
                rel:"stylesheet" ,
                href:"https://fonts.googleapis.com/css?family=Sofia&effect=neon|outline|emboss|shadow-multiple",
            }
             style { [include_str!("./css/fileexplorer.css")] }
              style { [include_str!("./fonts/roboto.css")] }
           
            body{
               match *scene {
                Scene::databaseList => rsx!(
                    header {
                        h1 {"NYMUT"}
                    }
                    aside{
                    p { "Start storing your passwords securely im a keePassXC database" }
                    button {  onclick: move |_| scene.set(Scene::NewDatabaseForm), "Create new database" }
                    label{ r#for:"file-upload" ,class:"custom-file-upload1", "Open existing database"}
                    input {class:"inputnavindex",
                                    r#type: "file",
                                    id:"file-upload",
                                    value: "{keyFile}",
                                    onchange: move |formdata| { println!("{:?}", formdata.data) },
                                    oninput: move |formdata| {
                                        println!("{:?}", formdata.data);
                                        scene.set(Scene::Settings);                       
                                    },
                    }
                    form { 
                        div { 
                            database.read().iter().map(|Database| rsx!(
                                div { 
                                     table{
                                        tr{
                                            th{class:"th1","Decryption Time"}
                                            td{class:"th2","{value} s"}
                                        }
                                        tr{
                                            th{class:"th1","key File"}
                                            td{class:"th2","{Database.key_File}"}
                                        }
                                      }
                                })
                            )
                        }
                    } 
                }
                ),
                Scene::NewDatabaseForm => rsx!(
                    main { 
                      h3 {  "DateBase Credentials" }
                      section{class:"container",
                          p { "A set Credentials known only to you that protects your database" }
                          h3 {  "Passwords" }
                          form { 
                            div{
                                 p { class:"p1input", "Enter passwords:" }
                                 input {
                                 r#type: "password",
                                 placeholder: "Passwords",
                                 value: "{Enterpasswords}",
                                 oninput: move |e| Enterpasswords.set(e.value.clone())
                                 }
                            }
                            div{
                                 p {class:"p2input", "Confirm passwords:" }
                                 input {
                                 r#type: "password",
                                 value: "{Confirmpasswords}",
                                 background_color: "{password_verify}",
                                 oninput: move |e| {
                                        Confirmpasswords.set(e.value.clone());
                                        if e.value != *Enterpasswords {
                                        password_verify.set("red".to_owned()) 
                                        } else {
                                         password_verify.set("white".to_owned());
                                          
                                        }
                                    }
                                 }
                            }

                          }
                          button{ a { class:"adisplaynone", href:"#id01","Add addotopnal protection..." }}
                         
                          nav{id:"id01", class:"msg",
                          h3 {  "key File" }
                          form { 
                            div{
                                 p { class:"p3input", "key File:" }
                                 label{ r#for:"file-upload" ,class:"custom-file-upload","Upload"}
                                 input {class:"inputnav1",
                                    r#type: "file",
                                    id:"file-upload",
                                    value: "{keyFile}",
                                    oninput: move |e| keyFile.set(e.value.clone())
                                 }
                            }
                          }
                          h3 { class:"msgh3" ,"Decryption" }
                          form {class:"msgh3" ,
                            div{ class:"slidecontainer",
                                 p {class:"p3input", "Decryption Time: {value} s" }
                                 input{  r#type:"range", min:"1", max:"100", value:"{value}" ,class:"slider" ,id:"myRange",  
                                 oninput: move |e| value.set(e.value.clone())}
                            }
                          }
                          }
                      }
                    }
                    footer{
                            button {class:"footerbuttonAddnew", 
                            onclick: move |_| {
                                database.write().push(Database {
                                  
                                    Enter_passwords: (*Enterpasswords).clone(),
                                    Confir_mpasswords: (*Confirmpasswords).clone(),
                                    key_File: (*keyFile).clone(),
                                });
                              
                                Enterpasswords.set(String::new());
                                Confirmpasswords.set(String::new());
                                keyFile.set(String::new());
                            },
                            "Add New"
                            }
                            button { onclick: move |_| scene.set(Scene::databaseList),
                            "Go Back"
                            }
                    }
                ),
                Scene::Settings => rsx!(
                    main {
                        h3 {  "open existing database" }
                        section{class:"container",
                           p { "A set Credentials known only to you that protects your database" }
                            /*database.read().iter().map(|Database| rsx!(
                                div {
                                    p {class: "pliste", "{Database.key_File}" }
                                })
                            )*/
                           h3 {  "Passwords" }
                           form { 
                               div{
                                   p { class:"p1input", "Enter passwords  :" }
                                   input {
                                   r#type: "password",
                                       placeholder: "Enter passwords",
                                       value: "{Enterpasswords}",
                                       oninput: move |e| Enterpasswords.set(e.value.clone())
                                   }
                               }
                               div{
                                   p { class:"p4input", "key File:"  }
                                   /*input {
                                        r#type: "file",
                                        onchange: move |formdata| { println!("{:?}", formdata.data) },
                                   }*/
                                   input {
                                       r#type: "text",
                                       placeholder: "key File",
                                       value: "{keyFile}",
                                       oninput: move |e| keyFile.set(e.value.clone())
                                   }
                               }
                           }
                        }
                    }
                    footer{
                             button {class:"footerbuttonAddnew",  onclick: move |_| scene.set(Scene::databaseList),
                             "ok"}
                             button { onclick: move |_| scene.set(Scene::databaseList),
                             "Cancel"
                             }
                    }
                ),
                Scene::keyFil => rsx!(
                     p { "Enter passwords  :" }
                )
               }
            }
        }
    ))
}