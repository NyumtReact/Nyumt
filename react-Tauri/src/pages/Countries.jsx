import React,{useEffect, useState} from "react";
import customerList from '../assets/JasorData/Countries.-data.json'

import { useRef} from 'react'
function Countries() {

    const clickOutsodeRef = (content_ref, toggle_ref) => {
        document.addEventListener('mousedown', (e) =>{
            //user click toggle
            if(toggle_ref.current && toggle_ref.current.contains(e.target)) {
                content_ref.current.classList.toggle('active')
            }
            else{
                //user click outside toggle and content
                if(content_ref.current && !content_ref.contains(e.target)){
                    content_ref.current.classList.remove('active')
                }
            }
        })
    }



    const menu_ref =useRef(null)
    const menu_toggle_ref =useRef(null)

    clickOutsodeRef(menu_ref, menu_toggle_ref)

    const setActiveMenu = () => menu_ref.current.classList.add('active')

    const closeMenu = () => menu_ref.current.classList.remove('active')

    const [userData, setUserData] = useState(customerList);
    const [users, setUsers] = useState([])

    useEffect(() => {
        setUsers(userData)
    }, [])

    const handleChange = (e) => {
        const {name,  checked} = e.target;
        if(name=== "allSelect"){
            let tempUser = users.map(user => {
                return {...user, ischecked: checked};
            });
            setUsers(tempUser)
        }else{
            let tempuser = users.map((user) => 
            user.name === name ? {...user, ischecked: checked} : user
            );
            setUsers(tempuser)
        }
      
    };
    return(
        <div className="col-12">
        <div className="card card2">
            <div className="card__body">
                <form>
                    <h3>select Users</h3>
                    <div>
                    <button ref={menu_toggle_ref}  onClick={() => setActiveMenu()}><i className='bx bx-sort'></i></button>
                        <input 
                            type="checkbox" 
                            name="allSelect"  
                            checked={users.filter(users => users?.ischecked !== true).length < 1}
                            onChange={handleChange}/>
                        <label htmlFor="">All select</label>
                    </div>
                    <div ref={menu_ref} className="select-All" >
                    {
                        users.map((user)=>(
                        <div>
                            <input
                                type="checkbox" 
                                name={user.name} 
                                checked={user?.ischecked || false}
                                onChange={handleChange}
                             />
                            <label htmlFor="">{user.name}</label>
                        </div>
                        )) 
                    }
                    </div>
                </form>
            </div>
        </div>
    </div>
    );
}
export default Countries;